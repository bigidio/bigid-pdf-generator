import express from "express";
import reports from "./routes/reports";

const app = express();

const port = process.env.PORT || 3011;

app.use('/api', reports);

app.listen(port, function listenHandler() {
  console.info(`Running on ${port}`)
});


