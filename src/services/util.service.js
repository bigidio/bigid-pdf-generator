import { Readable } from 'stream'

export const getBufferAsStream = (buffer) => {
  const readable = new Readable();
  readable._read = () => {};
  readable.push(buffer);
  readable.push(null);
  return readable;
};

export const sendPdfAsStream = (res, pdf, fileName) => {
  
  res.setHeader('Content-Type', 'application/pdf');
  res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
  
  res.set({
    'Content-Type': 'application/pdf',
    'Content-Disposition': `attachment; filename=${fileName || 'pdf-report'}.pdf`,
  });
  
  getBufferAsStream(pdf).pipe(res);
};