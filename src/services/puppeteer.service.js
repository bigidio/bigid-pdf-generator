import puppeteer from 'puppeteer';

const defaultPDFOptions = {
  format: 'A4',
  printBackground: true,
  margin: {
    left: '0px',
    top: '0px',
    right: '0px',
    bottom: '0px',
  },
};

let browser = null;

export const createPDFFromHtmlString = async (content, options = defaultPDFOptions) => {
  let page;
  
  try {
    
    if (!browser) {
      browser = await puppeteer.launch({ headless: true });
      console.log('init headless browser');
    }
    
    page = await browser.newPage();
    await page.setContent(content);
    return await page.pdf(options);
  } catch (e) {
    console.log(e);
    throw e;
  } finally {
    page && page.close();
  }
};