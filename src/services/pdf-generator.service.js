import React from "react";
import {renderToString} from "react-dom/server"
import { createPDFFromHtmlString } from './puppeteer.service';

export const createPDFFromTemplate = async (Template, props = {}) => {
  const templateAsHtmlString = renderToString(<Template { ...props } />);
  return await createPDFFromHtmlString(templateAsHtmlString);
};