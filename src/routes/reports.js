import React from 'react';
import express from "express";
import { sendPdfAsStream } from '../services/util.service';
import { createPDFFromTemplate } from '../services/pdf-generator.service';
const router = express.Router();

router.get('/', async (req, res) => {
  const props = {text: 'test me'};
  const pdf = await createPDFFromTemplate(ExamplePDFTemplate, props);
  sendPdfAsStream(res, pdf, 'an example');
})
;

function ExamplePDFTemplate({ text = "" }) {
  return (
    <div>
      <div>{`Someday I will be a pdf!`}</div>
      <div>{text}</div>
    </div>
  );
}


export default router;